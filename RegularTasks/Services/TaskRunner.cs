﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using RegularTasks.Models;

namespace RegularTasks.Services
{
    public interface ITaskRunner
    {
        IEnumerable<TestResult> Execute(Task task, string pattern, string replaceText = null);        
    }

    public class TaskRunner : ITaskRunner
    {
        public IEnumerable<TestResult> Execute(Task task, string pattern, string replaceText = null)
        {
            switch (task.Mode)
            {
                case TaskMode.Replace:
                    return ExecuteReplace(task, pattern, replaceText);                    
                case TaskMode.Match:
                    return ExecuteMatch(task, pattern);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private IEnumerable<TestResult> ExecuteMatch(Task task, string pattern)
        {
            Debug.Assert(task.Mode == TaskMode.Match);
            Regex regex = null;
            try
            {
                regex = new Regex(pattern);
            }
            catch (Exception)
            {

            }

            foreach (var test in task.Tests)
            {
                bool? isPassed = null;
                try
                {
                    isPassed = regex.IsMatch(test.Text);
                }
                catch (Exception)
                {

                }

                yield return new TestResult
                {
                    Text = test.Text,       
                    Match = test.Match,
                    IsPassed = isPassed
                };
            }
        }

        private IEnumerable<TestResult> ExecuteReplace(Task task, string pattern, string replaceText)
        {
            Debug.Assert(task.Mode == TaskMode.Replace);
            Regex regex = null;
            try
            {
                regex = new Regex(pattern);
            }
            catch (Exception)
            {

            }

            foreach (var test in task.Tests)
            {
                string actualText = null;
                try
                {
                    actualText = regex.Replace(test.Text, replaceText);
                }
                catch (Exception)
                {

                }

                yield return new TestResult
                {
                    Text = test.Text,
                    ExpectedText = test.ExpectedText,
                    ActualText = actualText,
                    IsPassed = actualText == test.ExpectedText
                };
            }
        }
    }
}