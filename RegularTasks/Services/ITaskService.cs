﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using RegularTasks.Data;
using RegularTasks.Models;

namespace RegularTasks.Services
{
    public interface ITaskService
    {
        IEnumerable<Task> GetTasks();

        Task GetTask(Guid id);
        IEnumerable<TestResult> Execute(Guid id, string pattern, string replaceText);
    }

    public class DebugTaskService : ITaskService
    {
        public ITaskRunner Runner { get; }

        public DebugTaskService(ITaskRunner runner)
        {
            Runner = runner;
        }

        public IEnumerable<Task> GetTasks()
        {
            return _tasks;
        }

        private static Task[] _tasks = new[]
        {
            new Task()
            {
                Id = Guid.NewGuid(),
                Name = "Задача1",
                Description = "Описание1",
                Tests =
                {
                    new Test
                    {
                        Id = Guid.NewGuid(),
                        Text = "0.1 0.02",
                        ExpectedText = "0,1 0,02"
                    },
                    new Test
                    {
                        Id = Guid.NewGuid(),
                        Text = "Текст. 0.1 0.02",
                        ExpectedText = "Текст. 0,1 0,02"
                    }
                }
            },
            new Task()
            {
                Id = new Guid("BCFB2630-F7B0-4B3E-B182-62DADC5350B3"),
                Name = "Задача2",
                Description = "Описание2"
            },
            new Task()
            {
                Id = Guid.NewGuid(),
                Name = "Задача3"
            },
            new Task()
            {
                Id = Guid.NewGuid(),
                Name = "Задача4"
            },
        };

        public Task GetTask(Guid id)
        {
            return GetTasks().FirstOrDefault(t => t.Id == id);
        }

        public IEnumerable<TestResult> Execute(Guid id, string pattern, string replaceText)
        {
            return Runner.Execute(GetTask(id), pattern, replaceText);
        }
    }

    public class TaskService : ITaskService
    {
        private readonly ITaskRunner _runner;
        private readonly DbContextOptions<ApplicationDbContext> _dbContextOptions;

        public TaskService(ITaskRunner runner, DbContextOptions<ApplicationDbContext> dbContextOptions)
        {
            _runner = runner;
            _dbContextOptions = dbContextOptions;
        }

        public IEnumerable<Task> GetTasks()
        {
            using (var db = new ApplicationDbContext(_dbContextOptions))
            {
                return db.Tasks.ToList();
            }
        }

        public Task GetTask(Guid id)
        {
            using (var db = new ApplicationDbContext(_dbContextOptions))
            {
                var task = db.Tasks.Find(id);
                task.Tests = db.Tests.Where(t => t.TaskId == task.Id).OrderBy(t => t.Order).ToList();                
                return task;
            }
        }

        public IEnumerable<TestResult> Execute(Guid id, string pattern, string replaceText)
        {
            return _runner.Execute(GetTask(id), pattern, replaceText);
        }
    }
}