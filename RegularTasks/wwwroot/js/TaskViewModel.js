﻿// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

function TaskViewModel(id, tests) {
    var storedData = JSON.parse(localStorage.getItem(id)) || {};    
    
    this.pattern = ko.observable(storedData.pattern);
    this.replace = ko.observable(storedData.replace);    

    this.tests = ko.observableArray(tests);

    var me = this;      

    function request() {
        $.ajax({
            url: "/Task/ExecuteTask",
            data: {
                id: id,
                replaceText: me.replace(),
                pattern: me.pattern()
            },
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                me.tests(data);

                localStorage.setItem(id, JSON.stringify({
                    pattern: me.pattern(),
                    replace: me.replace()
                }));
            }
        });
    };

    var debouncedRequest = debounce(request, 1000);

    this.pattern.subscribe(debouncedRequest);
    this.replace.subscribe(debouncedRequest);

    // При первом открытии страницы выполняем запрос только в том 
    // случае, если шаблон или строка замены не пустые
    if (this.pattern() || this.replace())
        this.pattern.valueHasMutated();    
}        