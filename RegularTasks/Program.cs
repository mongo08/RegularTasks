﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RegularTasks.Data;

namespace RegularTasks
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);
            
            using (var scope = host.Services.GetRequiredService<IServiceProvider>().CreateScope())
            {
                var logger = scope.ServiceProvider.GetService<ILogger<Program>>();
                try
                {
                    DataInitializer.Seed(scope.ServiceProvider);
                }
                catch (Exception ex)
                {
                    logger.LogError("Ошибка при заполнении базы данных", ex);
                }
            }

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
