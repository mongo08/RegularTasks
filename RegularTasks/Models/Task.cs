﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RegularTasks.Models
{
    public class Task
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskMode Mode { get; set; }
        public ICollection<Test> Tests { get; set; } = new List<Test>();
    }

    public enum TaskMode
    {
        Replace,
        Match
    }

    public class Test
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public string ExpectedText { get; set; }
        
        public Guid TaskId { get; set; }
        public int Order { get; set; }
        public bool? Match { get; set; }
    }

    public class TestResult
    {
        public string Text { get; set; }
        public string ExpectedText { get; set; }
        public bool? Match { get; set; }
        public string ActualText { get; set; }
        public bool? IsPassed { get; set; }
    }
}