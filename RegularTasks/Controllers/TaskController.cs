﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using RegularTasks.Models;
using RegularTasks.Services;

namespace RegularTasks.Controllers
{
    public class TaskController : Controller
    {
        private readonly ITaskService _taskService;

        public TaskController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        // GET
        public IActionResult Index()
        {
            var tasks = _taskService.GetTasks();
            return View(tasks);
        }

        public IActionResult Task(Guid id)
        {
            var task = _taskService.GetTask(id);            
            return View(task);
        }

        public TestResult[] ExecuteTask(Guid id, string pattern, string replaceText)
        {
            return _taskService.Execute(id, pattern, replaceText).ToArray();            
        }
    }
}