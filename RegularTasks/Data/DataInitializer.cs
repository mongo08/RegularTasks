﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RegularTasks.Models;

namespace RegularTasks.Data
{
    public class DataInitializer
    {
        public static void Seed(IServiceProvider provider)
        {
            using (var db = new ApplicationDbContext(provider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {                                
                IEnumerable<Task> tasks = new []
                {
                    CreateTask("E0CAEFC2-4F63-4615-AEEB-D6D188D96655", TaskMode.Replace, "Repeated words", @"<p>This weeks Regex Tuesday challenge, the first challenge, is to make a regex which finds and highlights repeated words in a body of text. It is a fairly easy challenge, and future challenges will usually be trickier.</p><p>You should just wrap the repeated word in a &lt;strong&gt; element. For example, <code>this is is a test</code> should be turned into <code>this is &lt;<strong>is</strong>&gt; a test</code>.</p><p>To test a regular expression on the test cases below, type it into the text input. Each test case will be marked as passed or failed respectively - you are aiming to get as many test cases as you can to pass. Note that JavaScript must be enabled for this feature to work. The regex engine used is the JavaScript regex engine; it is similar to PCRE, but with a few differences.</p>", new []
                    {
                        CreateTest("This is a test", "This is a test"),
                        CreateTest("This is is a test", "This is <strong>is</strong> a test"),
                        CreateTest("This test test is is", "This test <strong>test</strong> is <strong>is</strong>"),
                        CreateTest("This test is a test", "This test is a test"),
                        CreateTest("This this test is a test", "This <strong>this</strong> test is a test"),
                        CreateTest("cat dog dog cat dog", "cat dog <strong>dog</strong> cat dog"),
                        CreateTest("This test is a test tester", "This test is a test tester"),
                        CreateTest("hello world hello world", "hello world hello world"),
                        CreateTest("This nottest test is something", "This nottest test is something"),
                        CreateTest("This is IS a test", "This is <strong>IS</strong> a test"),
                        CreateTest("<Mack> I'll I'll be be back back in in a a bit bit.", "<Mack> I'll <strong>I'll</strong> be <strong>be</strong> back <strong>back</strong> in <strong>in</strong> a <strong>a</strong> bit <strong>bit</strong>."),                        
                    }),
                    CreateTask("35EC103C-EAF3-44F8-BBAF-35C7B0D7DFBF", TaskMode.Match, "Grayscale colors", @"<p>The second Regex Tuesday challenge is slightly trickier challenge. Unlike the last challenge, it is to match something, not to replace something.</p><p>In this challenge, you are aiming to match a grayscale CSS colour. It should be able to match anything but the colour names (eg ""red""). See the test cases below for examples.</p><p>To test a regular expression on the test cases below, type it into the text input. Each test case will be marked as passed or failed respectively - you are aiming to get as many test cases as you can to pass. Note that JavaScript must be enabled for this feature to work.The regex engine used is the JavaScript regex engine; it is similar to PCRE, but with a few differences.</p><p>If you're finding this challenge too tricky, you can simplify it - for example, to match all the hex colours (eg ""#000""), and then move onto the rgb colours. hsl can be a bit trickier - do as much or as little as you can. For an explanation of the various colour syntaxes available, check out this article. Good luck!</p>", new[]
                    {
                        CreateTest("#000", true),
                        CreateTest("#aaa", true),
                        CreateTest("#eEe", true),
                        CreateTest("#111111", true),
                        CreateTest("#6F6F6F", true),
                        CreateTest("#efEfEF", true),
                        CreateTest("rgb(0, 0, 0)", true),
                        CreateTest("rgb(15,15,15)", true),
                        CreateTest("rgb(2.5, 2.5,2.5)", true),
                        CreateTest("rgb(1, 01, 000001)", true),
                        CreateTest("rgb(20%, 20%,20%)", true),
                        CreateTest("rgba(4,4,4,0.8)", true),
                        CreateTest("rgba(4,4,  4,1 )", true),
                        CreateTest("rgba(3,3,3,0.12536)", true),
                        CreateTest("rgba(10%,10%,10%,5%)", true),
                        CreateTest("hsl(20,0%,  50%)", true),
                        CreateTest("hsl(0, 10%, 100%)", true),
                        CreateTest("hsl(0.5, 10.5%, 0%)", true),
                        CreateTest("hsl(5, 5%, 0%)", true),
                        CreateTest("hsla(20, 0%, 50%, 0.88)", true),
                        CreateTest("hsla(0, 0%, 0%, 0.25)", true),
                        CreateTest("#ef4", false),
                        CreateTest("#eEf", false),
                        CreateTest("#11111e", false),
                        CreateTest("#123456", false),
                        CreateTest("rgb(2, 4, 7)", false),
                        CreateTest("rgb(10, 10,100)", false),
                        CreateTest("rgb(1.5%, 1.5%, 1.6%)", false),
                        CreateTest("rgba(1, 01, 0010, 0.5)", false),
                        CreateTest("hsl(20, 20%, 20%)", false),
                        CreateTest("hsl(0, 1%, 01%)", false),
                        CreateTest("hsla(0, 10%, 50%, 0.5)", false),
                        CreateTest("#11111", false),
                        CreateTest("#000000000", false),
                        CreateTest("rgb (1, 1, 1)", false),
                        CreateTest("rgb(10, 10, 10, 10)", false),
                        CreateTest("rgb(257, 257, 257)", false),
                        CreateTest("rgb(10%, 10, 10)", false),
                        CreateTest("argb(1,1,1)", false),
                    }),
                    CreateTask("00204BA7-1038-416A-8D33-5070525A6FBB", TaskMode.Replace, "Italic MarkDown", @"<p>This weeks Regex Tuesday Challenge is to write part of a MarkDown parser - turn italic MarkDown (*this is italic*) into HTML italic: <em>this is italic</em>. It should not, however, match bold text - text surrounded by multiple asterisks.</p><p>This is a somewhat unrealistic challenge - in real life, you wouldn't have to make sure that it isn't bold, as you would have already parsed the bold text.</p><p>To test a regular expression on the test cases below, type it into the text input. Each test case will be marked as passed or failed respectively - you are aiming to get as many test cases as you can to pass. Note that JavaScript must be enabled for this feature to work. The regex engine used is the JavaScript regex engine; it is similar to PCRE, but with a few differences.</p>", new []
                    {
                        CreateTest("This text is not italic.", "This text is not italic."),
                        CreateTest("*This text is italic.*", "<em>This text is italic.</em>"),
                        CreateTest("This text is *partially* italic", "This text is <em>partially</em> italic"),
                        CreateTest("This text has *two* *italic* bits", "This text has <em>two</em> <em>italic</em> bits"),
                        CreateTest("**bold text (not italic)**", "**bold text (not italic)**"),
                        CreateTest("**bold text with *italic* **", "**bold text with <em>italic</em> **"),
                        CreateTest("**part bold,** *part italic*", "**part bold,** <em>part italic</em>"),
                        CreateTest("*italic text **with bold** *", "<em>italic</em> **bold** <em>italic</em> **bold**"),
                        CreateTest("*italic* **bold** *italic* **bold**", "<em>italic</em> **bold** <em>italic</em> **bold**"),
                        CreateTest("*invalid markdown (do not parse)**", "*invalid markdown (do not parse)**"),
                        CreateTest("random *asterisk", "random *asterisk"),
                    }),
                };

                //if(!db.Tasks.Any())
                //    db.Tasks.AddRange(tasks);                
                foreach (var task in tasks)
                    AddOrUpdateTask(db, task);

                db.SaveChanges();
            }
        }

        private static Test CreateTest(string text, bool match)
        {
            return new Test
            {
                Id = Guid.NewGuid(),
                Text = text,
                Match = match
            };
        }

        private static Test CreateTest(string text, string expectedText)
        {
            return new Test
            {
                Id = Guid.NewGuid(),
                Text = text,
                ExpectedText = expectedText
            };
        }

        private static Task CreateTask(string id, TaskMode mode, string name, string description, IEnumerable<Test> tests)
        {
            return new Task
            {
                Id = new Guid(id),
                Mode = mode,
                Name = name,
                Description = description,
                Tests = tests.Select((t,i)=>
                {
                    t.Order = i;
                    return t;
                }).ToList()
            };
        }

        private static void AddOrUpdateTask(ApplicationDbContext ctx, Task task)
        {
            var entry = ctx.Tasks.Find(task.Id);
            if (entry != null)
            {
                ctx.Update(task);                
            }
            else
            {
                ctx.Add(task);
            }
        }
    }
}